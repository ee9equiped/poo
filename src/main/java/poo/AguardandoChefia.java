package poo;

import entities.annotations.EntityDescriptor;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@ToString
@EntityDescriptor(hidden = true, pluralDisplayName = "Aguardando Chefia")
public class AguardandoChefia extends Status {
    public AguardandoChefia() {
        id = 2L;
    }

    @Override
    public void solicitar() {
        throw new IllegalStateException("Esta solicitação já foi realizada!");
    }

    @Override
    public void aprovar() {
        solicitacao.status = new AguardandoRH();
    }

    @Override
    public void recusar() {
        solicitacao.status = new Recusada();
    }

    @Override
    public void retornar(String motivo) {
        throw new IllegalStateException("Esta operação não está disponível!");
    }

    public void cancelar() {
        solicitacao.status = new Cancelado();
    }
}
