package poo;

import entities.annotations.EntityDescriptor;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@ToString
@EntityDescriptor(hidden = true, pluralDisplayName = "Aguardando RH")
public class AguardandoRH extends Status {
    public AguardandoRH() {
        id = 3L;
    }

    @Override
    public void solicitar() {
        throw new IllegalStateException("Esta solicitação já foi realizada!");
    }

    @Override
    public void aprovar() {
        solicitacao.status = new Aprovada();
    }

    @Override
    public void recusar() {
        solicitacao.status = new Recusada();
    }

    @Override
    public void retornar(String motivo) {
        if (motivo == null) {
            throw new IllegalArgumentException("O motivo está nulo");
        }
        solicitacao.setMotivo(motivo);
        solicitacao.status = new AguardandoChefia();
    }

    public void cancelar() {
        solicitacao.status = new Cancelado();
    }
}
