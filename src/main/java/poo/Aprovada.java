package poo;

import entities.annotations.EntityDescriptor;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@ToString
@EntityDescriptor(hidden = true, pluralDisplayName = "Aprovada")
public class Aprovada extends Status {
    public Aprovada() {
        id = 4L;
    }

    public void solicitar() {
        throw new IllegalStateException("Esta solicitação já foi realizada!");
    }

    public void aprovar() {
        throw new IllegalStateException("Esta solicitação já foi aprovada!");
    }

    public void recusar() {
        throw new IllegalStateException("Não é possível recusar. Esta solicitação já foi aprovada!");
    }

    public void retornar(String motivo) {
        solicitacao.status = new AguardandoRH();
    }

    public void cancelar() {
        throw new IllegalStateException("Não é possível cancelar esta solicitação já foi aprovada!");
    }
}
