package poo;

import entities.annotations.EntityDescriptor;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@ToString
@EntityDescriptor(hidden = true, pluralDisplayName = "Cancelado")
public class Cancelado extends Status {
    public Cancelado() {
        id = 6L;
    }

    public void solicitar() {
        throw new IllegalStateException("Esta solicitação está cancelada!");
    }

    public void aprovar() {
        throw new IllegalStateException("Esta solicitação está cancelada!");
    }

    public void recusar() {
        throw new IllegalStateException("Esta solicitação está cancelada!");
    }

    public void retornar(String motivo) {
        throw new IllegalStateException("Esta solicitação está cancelada!");
    }

    public void cancelar() {
        throw new IllegalStateException("Esta solicitação está cancelada!");
    }
}
