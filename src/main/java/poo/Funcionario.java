package poo;

import entities.annotations.View;
import entities.annotations.Views;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Data
@Views({
        @View(name = "Funcionarios",
                title = "Funcionários",
                members = "['':photo,[nome;cpf;username;password]]",
                template = "@CRUD",
                roles = "RH")})
public class Funcionario extends User implements Serializable {
    @Column(length = 11, nullable = false)
    @CPF
    private String cpf;

    @Column(length = 60, nullable = false)
    private String nome;

    public Funcionario() {
        getRoles().clear();
        getRoles().add(Role.Empregado);
    }

    public String toString() {
        return this.nome;
    }
}
