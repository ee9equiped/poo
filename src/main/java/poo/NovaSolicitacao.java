package poo;

import entities.annotations.EntityDescriptor;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@ToString
@EntityDescriptor(hidden = true, pluralDisplayName = "Nova Solicitação")
public class NovaSolicitacao extends Status {
    public NovaSolicitacao() {
        id = 1L;
    }

    @Override
    public void solicitar() {
        if (!solicitacao.getTipoDeSolicitacao().equals(Solicitacao.TipoDeSolicitacao.Atestado)
                && !solicitacao.getTipoDeSolicitacao().equals(Solicitacao.TipoDeSolicitacao.ProblemaPonto))
            solicitacao.status = new AguardandoChefia();
        else
            solicitacao.status = new AguardandoRH();
    }

    @Override
    public void aprovar() {
        throw new IllegalStateException("Esta operação não está disponível!");
    }

    @Override
    public void recusar() {
        throw new IllegalStateException("Esta operação não está disponível!");
    }

    @Override
    public void retornar(String motivo) {
        throw new IllegalStateException("Esta operação não está disponível!");
    }

    @Override
    public void cancelar() {
        throw new IllegalStateException("Esta operação não está disponível!");
    }
}
