package poo;

import entities.annotations.EntityDescriptor;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@ToString
@EntityDescriptor(hidden = true, pluralDisplayName = "Recusada")
public class Recusada extends Status {
    public Recusada() {
        id = 5L;
    }

    public void solicitar() {
        throw new IllegalStateException("Esta operação não está disponível");
    }

    public void aprovar() {
        throw new IllegalStateException("Esta operação não está disponível");
    }

    public void recusar() {
        throw new IllegalStateException("Esta operação não está disponível");
    }

    public void retornar(String motivo) {
        throw new IllegalStateException("Esta operação não está disponível");
    }

    public void cancelar() {
        throw new IllegalStateException("Esta operação não está disponível");
    }
}
