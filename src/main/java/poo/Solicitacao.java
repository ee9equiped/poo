package poo;

import entities.annotations.View;
import entities.annotations.Views;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Date;

@Entity
@ToString
@NamedQueries({
        @NamedQuery(
                name = "SolicitacoesAguardandoChefia",
                query = "Select s"
                        + " From Solicitacao s, AguardandoChefia st"
                        + " Where s.status = st"),
        @NamedQuery(
                name = "SolicitacoesAguardandoRH",
                query = "Select s"
                        + " From Solicitacao s, AguardandoRH st"
                        + " Where s.status = st")
})
@Views({
        @View(name = "AbonarFaltas",
                title = "Abonar Faltas",
                namedQuery = "SolicitacoesAguardandoRH",
                members = "[*funcionario;*inicio;*termino],"
                        + "*motivo,"
                        + "[aprovar();recusar();retornar();cancelar();]",
                roles = "RH"),
        @View(name = "AprovacaoDaChefia",
                title = "Analisar Solicitações",
                namedQuery = "SolicitacoesAguardandoChefia",
                members = "[*funcionario:2;"
                        + " *inicio,*termino;*motivo,*observacao],"
                        + " [aprovar();recusar();cancelar();]",
                roles = "Chefia"),
        @View(name = "SolicitarAbono",
                title = "Solicitar Abono",
                namedQuery = "Select new poo.Solicitacao()",
                members = "[#funcionario;#inicio; "
                        + " #termino;#motivo;#tipoDeSolicitacao; "
                        + " #solicitar()]",
                roles = "Empregado")})
public class Solicitacao implements Serializable {
    @Getter
    @OneToOne(cascade = CascadeType.ALL)
    protected Status status = new NovaSolicitacao();
    @Id
    @GeneratedValue
    private Long id;
    @Getter
    @Setter
    @NotNull
    @ManyToOne(optional = false)
    private Funcionario funcionario;
    @Getter
    @Setter
    @Past
    @NotNull
    private Date inicio;
    @Getter
    @Setter
    @Past
    @NotNull
    private Date termino;
    @Getter
    @Setter
    @NotNull
    private String motivo;
    @Getter
    @Setter
    private String observacao;
    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoDeSolicitacao tipoDeSolicitacao;

    public String solicitar() {
        status.setSolicitacao(this).solicitar();
        Repositorio.getInstance().add(this);
        Repositorio.getInstance().persistAll();
        return "Solicitação realizada";
    }

    public String aprovar() {
        status.setSolicitacao(this).aprovar();
        Repositorio.getInstance().add(this);
        Repositorio.getInstance().persistAll();
        return "Solicitação aprovada";
    }

    public String recusar() {
        status.setSolicitacao(this).recusar();
        Repositorio.getInstance().add(this);
        Repositorio.getInstance().persistAll();
        return "Solicitação recusada";
    }

    public String retornar(String motivo) {
        status.setSolicitacao(this).retornar(motivo);
        Repositorio.getInstance().add(this);
        Repositorio.getInstance().persistAll();
        return "Solicitação retornada";
    }

    public String cancelar() {
        status.setSolicitacao(this).cancelar();
        Repositorio.getInstance().add(this);
        Repositorio.getInstance().persistAll();
        return "Solicitação cancelada";
    }

    public enum TipoDeSolicitacao {
        Servico, Atestado, Licenca, ProblemaPonto, Outros
    }
}
