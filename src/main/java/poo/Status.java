package poo;

import entities.annotations.EntityDescriptor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@Data
@EntityDescriptor(hidden = true, pluralDisplayName = "Statuses")
public abstract class Status implements Serializable {
    @Id
    protected Long id;
    Solicitacao solicitacao;

    @Transient
    public Solicitacao getSolicitacao() {
        return this.solicitacao;
    }

    protected Status setSolicitacao(Solicitacao solicitacao) {
        this.solicitacao = solicitacao;
        return this;
    }

    public abstract void solicitar();

    public abstract void aprovar();

    public abstract void recusar();

    public abstract void retornar(String motivo);

    public abstract void cancelar();
}
