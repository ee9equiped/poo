package poo;

import entities.Context;
import entities.Repository;
import entities.annotations.*;
import entities.descriptor.PropertyType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@EqualsAndHashCode(of = "username")
@Data
@NamedQueries({
        @NamedQuery(name = "Authentication",
                query = ""
                        + " From User u"
                        + " Where u.username = :username "
                        + " and u.password = :password "),
        @NamedQuery(name = "Administrators",
                query = ""
                        + " From User u "
                        + "Where 'Admin' in elements(u.roles)")
})
@Views({
        @View(
                name = "Users",
                title = "Users",
                members = "Usuário[username,newPhoto();"
                        + " password,newPassword();"
                        + " roles:2],*photo",
                template = "@CRUD+@PAGER",
                roles = "Admin"),
        @View(
                name = "Login",
                title = "Login",
                members = "[#username;#password;login()]",
                namedQuery = "Select new poo.User()",
                roles = "NOLOGGED"),
        @View(
                name = "Logout",
                title = "Logout",
                members = "['':*photo,[*username;*roles;[newPassword(),logout()]]]",
                namedQuery = "From User u Where u = :user",
                params = {@Param(name = "user", value = "#{context.currentUser}")},
                roles = "LOGGED")
})
@Table(name = "users")
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Lob
    @Editor(propertyType = PropertyType.IMAGE)
    private byte[] photo;

    @Column(length = 30, unique = true, nullable = false)
    @Username
    private String username;

    @Column(length = 32, nullable = false)
    @Type(type = "entities.security.Password")
    @PropertyDescriptor(secret = true, displayWidth = 25)
    private String password;

    @UserRoles
    @Enumerated(EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER)
    private List<Role> roles = new ArrayList<Role>();

    public User(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        getRoles().add(role);
    }

    static public String logout() {
        Context.clear();
        return "go:poo.User@Login";
    }

    public String login() {
        if (Repository.queryCount("Administrators") == 0) {
            User admin = new User("samuel", "123", Role.Admin);
            Repository.save(admin);
            Context.setCurrentUser(admin);
            return "go:poo.User@Users";
        } else {

            List<User> users = Repository.query("Authentication",
                    username,
                    password);
            if (users.size() == 1) {
                Context.setCurrentUser(users.get(0));
            } else {
                throw new SecurityException("Usuário/Senha invalidos!");
            }
        }
        return "go:home";
    }

    public String newPassword(
            @ParameterDescriptor(displayName = "New Password",
                    secret = true, required = true)
                    String newPassword,
            @ParameterDescriptor(displayName = "Confirm password",
                    secret = true, required = true)
                    String rePassword) {
        if (newPassword.equals(rePassword)) {
            this.setPassword(newPassword);
            Repository.save(this);
            return "Password changed successfully!";
        } else {
            throw new SecurityException("The passwords are different!");
        }
    }

    public void newPhoto
            (@ParameterDescriptor(displayName = "New Photo")
                     byte[] newPhoto) {
        this.photo = newPhoto;
        if (id != null) {
            Repository.save(this);
        }
    }

    public enum Role {Admin, Chefia, RH, Empregado}

}

