package poo;

import org.junit.Before;
import org.junit.Test;


public class AguardandoChefiaTest {

    private AguardandoChefia aguardandoChefia;
    private Solicitacao solicitacao;

    @Test
    @Before
    public void cenario() {
        aguardandoChefia = new AguardandoChefia();
    }

    @Test(expected = IllegalStateException.class)
    public void solicitarTest() {
        aguardandoChefia.solicitar();
    }

    @Test
    public void aprovarTest() {

    }

    @Test
    public void recusarTest() {

    }

    @Test(expected = IllegalStateException.class)
    public void retornarTest() {
        aguardandoChefia.retornar("motivo");
    }
}
