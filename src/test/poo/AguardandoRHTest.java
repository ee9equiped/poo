package poo;

import org.junit.Before;
import org.junit.Test;


public class AguardandoRHTest {

    private AguardandoRH aguardandoRH;
    private Solicitacao solicitacao;

    @Test
    @Before
    public void cenario() {
        solicitacao = new Solicitacao();
        aguardandoRH = new AguardandoRH();
    }

    @Test(expected = IllegalStateException.class)
    public void solicitarTest() {
        aguardandoRH.solicitar();
    }

    @Test
    public void aprovarTest() {

    }

    @Test
    public void recusarTest() {

    }

    @Test
    public void retornarTest() {

    }
}
