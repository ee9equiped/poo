package poo;

import org.junit.Before;
import org.junit.Test;

public class AprovadaTest {

    private Aprovada aprovada;

    @Test
    @Before
    public void cenario(){
        aprovada = new Aprovada();
    }

    @Test(expected = IllegalStateException.class)
    public void solicitarTest() {
        aprovada.solicitar();
    }

    @Test(expected = IllegalStateException.class)
    public void aprovarTest() {
        aprovada.aprovar();
    }

    @Test(expected = IllegalStateException.class)
    public void recusarTest() {
        aprovada.recusar();
    }

    @Test(expected = IllegalStateException.class)
    public void retornarTest() {
        aprovada.retornar("motivo");
    }
}
