package poo;

import org.junit.Before;
import org.junit.Test;


public class NovaSolicitacaoTest {

    private NovaSolicitacao novaSolicitacao;
    private Solicitacao solicitacao;

    @Test
    @Before
    public void cenario() {
        solicitacao = new Solicitacao();
        novaSolicitacao = new NovaSolicitacao();
    }

    @Test
    public void solicitarTest() {
        solicitacao.solicitar();
        assert solicitacao.getStatus().getClass().equals(new AguardandoChefia().getClass());
    }

    @Test(expected = IllegalStateException.class)
    public void aprovarTest() {
        novaSolicitacao.aprovar();
    }

    @Test(expected = IllegalStateException.class)
    public void recusarTest() {
        novaSolicitacao.recusar();
    }

    @Test(expected = IllegalStateException.class)
    public void retornarTest() {
        novaSolicitacao.retornar("motivo");
    }
}
