package poo;

import org.junit.Before;
import org.junit.Test;

public class RecusadaTest {

    private Recusada recusada;

    @Test
    @Before
    public void cenario() {
        recusada = new Recusada();
    }

    @Test(expected = IllegalStateException.class)
    public void solicitarTest() {
        recusada.solicitar();
    }

    @Test(expected = IllegalStateException.class)
    public void aprovarTest() {
        recusada.aprovar();
    }

    @Test(expected = IllegalStateException.class)
    public void recusarTest() {
        recusada.recusar();
    }

    @Test(expected = IllegalStateException.class)
    public void retornarTest() {
        recusada.retornar("motivo");
    }
}
